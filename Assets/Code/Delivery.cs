using UnityEngine;

public class Delivery : MonoBehaviour
{
    private const float PICKUP_DELAY = .23f;
    
    private Color _hasPackageColor = Color.green;
    private Color _hasNoPackageColor = Color.white;

    private bool _haspackage = false;

    [SerializeField] Driver _driver;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Package") && !_haspackage)
        {
            _haspackage = true;

            Destroy(other.gameObject, PICKUP_DELAY);
            GetComponent<SpriteRenderer>().color = _hasPackageColor;
        }

        if (other.gameObject.CompareTag("Drop") && _haspackage)
        {
            _haspackage = false;

            GetComponent<SpriteRenderer>().color = _hasNoPackageColor;
        }

        if (other.gameObject.CompareTag("Boost"))
        {
            _driver.Boost();
            Destroy(other.gameObject, PICKUP_DELAY);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _driver.HadAccident();
    }

}
