using UnityEngine;

public class Camera : MonoBehaviour
{

    [SerializeField] private GameObject _player;

    void LateUpdate()
    {
        transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, -10);
    }
}
