using UnityEngine;

public class Driver : MonoBehaviour
{
    private float _steerSpeed = 325f;
    private float _movementSpeed = 14f;

    private float _speedModifier = 0;

    private bool _hasBoost = false;
    private bool _hadAccident = true;

    void Update()
    {
        Steer();
        Move();
    }

    private void Steer()
    {
        var horizontal = Input.GetAxisRaw("Horizontal") * Time.deltaTime * _steerSpeed;

        var _steeringDirection = horizontal;
        transform.Rotate(new Vector3(0, 0, -_steeringDirection));
    }

    private void Move()
    {
        var vertical = Input.GetAxisRaw("Vertical") * Time.deltaTime * (_movementSpeed + _speedModifier);

        var _moveDirection = vertical;
        transform.Translate(new Vector3(0, _moveDirection, 0));
    }

    public void Boost()
    {
        _speedModifier = 7;
    }

    public void HadAccident()
    {
        _speedModifier = -7;        
    }
}
